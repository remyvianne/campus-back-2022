CREATE DATABASE IF NOT EXISTS `back` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `back`;

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
COMMIT;