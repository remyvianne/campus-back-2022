var express = require('express');
var router = express.Router();
const mysql = require('mysql');

const db = mysql.createConnection({
    host: "mysql",
    // host: "localhost",
    port: "3306",
    user: "root",
    database: "back",
    password: "password"
    // password: ""
});

db.connect(function (err) {
    if (err) throw err;
    console.log("Connecté à la base de données MySQL!");
});

router.get('/', function (req, res, next) { getOneUser(req, res, next) });

router.get('/all', function (req, res, next) { getAllUsers(req, res, next) });

router.post('/', function (req, res, next) { insertOneUser(req, res, next) });

router.delete('/', function (req, res, next) { deleteOneUser(req, res, next) });



/**  --------------- FUNCTION AREA --------------- **/
function getOneUser(req, res, next) {
    db.query("SELECT * FROM user WHERE email = '" + req.body.email + "' AND password = '" + req.body.password + "'", function (err, result) {
        if (err) throw err;
        if (result.length === 0) {
            res.send(false)
        } else {
            res.send(result);
        }
    });
}
function getAllUsers(req, res, next) {
    db.query("SELECT * FROM user", function (err, result) {
        if (err) throw err;
        res.send(result);
    });
}

function insertOneUser(req, res, next) {
    if (req.body.email.match(new RegExp('[a-z0-9]+@[a-z]+\.[a-z]{2,3}'))) {

        db.query("SELECT * FROM user WHERE email = '" + req.body.email + "'", function (err, result) {
            if (result.length === 0) {
                if (req.body.password.length >= 5) {
                    db.query("INSERT INTO user (email, password) VALUES ('" + req.body.email + "', '" + req.body.password + "')", function (err, result) {
                        if (err) throw err;
                        res.send(result);
                    });
                } else {
                    res.send("Password must be at least 5 characters long");
                }
            } else {
                res.send("E-mail already used");
            }
        });
    } else {
        res.send("E-mail not valid");
    }

    // res.send("Utilisateur déjà existant");
}

function deleteOneUser(req, res, next) {
    db.query("DELETE FROM user WHERE email='" + req.body.email + "'", function (err, result) {
        if (err) throw err;
        res.send(result);
    });
}

module.exports = router;
